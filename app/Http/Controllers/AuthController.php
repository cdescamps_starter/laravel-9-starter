<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    // User connect to webApp    
    public function connect(AuthRequest $request)
    {
        // Get user from email
        $user = User::where('email', $request->email)->first();

        // Check password
        if (Hash::check($request->password, $user->password)) {
            // Token creation
            $token = $user->createToken('auth_token')->plainTextToken;

            return response()->json(['user' => $user, "token" => $token]);
        } else {
            return response()->json(["message" => "Identifiants incorrects"]);
        }
    }

    // Return authenticated User 
    public function me()
    {
        return Auth::user();
    }
}
