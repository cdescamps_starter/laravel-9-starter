<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Authentication
Route::controller(AuthController::class)->prefix('auth')->group(function () {
    Route::post('', 'connect');
    Route::post('me', "me")->middleware('auth:sanctum');
});


// Authenticated routes
Route::middleware('auth:sanctum')->group(function () {
    Route::apiResources([]);
});


Route::get('/', function () {
    return response()->json(['API works']);
});
